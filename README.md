# Energy consumption service
Service that is responsible for processing meter reading data from customers.

## Getting started
This project uses Gradle as build tool.
Run `./gradlew build` to build the project.
To start the application use the command `./gradlew bootRun`

## Application
The application will run at port 8080  

The h2 database can be accessed at http://localhost:8080/h2/

Swagger documentation for the application is at http://localhost:8080/swagger-ui.html

## Examples
Example JSON body for creating a new profile with fractions:
```json
[
	{
		"profileName": "ABC",
		"fractions": {
			"JAN": 0.1,
			"FEB": 0.1,
			"MAR": 0.1,
			"APR": 0.1,
			"MAY": 0.1,
			"JUN": 0.1,
			"JUL": 0.1,
			"AUG": 0.1,
			"SEP": 0.05,
			"OCT": 0.05,
			"NOV": 0.05,
			"DEC": 0.05
		}
	}
]
```
Example for creating meter readings with above profile:
```json
[
	{
		"meterId": "0001",
		"profile": "ABC",
		"readings": {
			"JAN": 10,
			"FEB": 20,
			"MAR": 30,
			"APR": 40,
			"MAY": 50,
			"JUN": 60,
			"JUL": 70,
			"AUG": 80,
			"SEP": 85,
			"OCT": 90,
			"NOV": 95,
			"DEC": 100
		}
	}
]
```
After this the consumption can be retrieved by a GET operation, 
eg: http://localhost:8080/consumption/0001/JAN

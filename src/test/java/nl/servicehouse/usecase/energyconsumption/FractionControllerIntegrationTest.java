package nl.servicehouse.usecase.energyconsumption;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.json.JSONException;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import nl.servicehouse.usecase.energyconsumption.common.Month;
import nl.servicehouse.usecase.energyconsumption.fraction.Fraction;
import nl.servicehouse.usecase.energyconsumption.fraction.FractionRepository;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = EnergyConsumptionApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class FractionControllerIntegrationTest {

    @Autowired
    private TestRestTemplate restTemplate;
    @Autowired
    private FractionRepository fractionRepository;

    private ParameterizedTypeReference<List<Fraction>> responseType = new ParameterizedTypeReference<>() {

    };

    @After
    public void tearDown() {
        //Manually clear the database
        //@Transactional doesnt work since we dont call services but do HTTP calls with RestTemplate
        fractionRepository.deleteAll();
    }

    @Test
    public void testGetFails() {
        ResponseEntity<String> response = restTemplate.getForEntity("/fractions/{id}", String.class, UUID.randomUUID());
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    public void testGetSucceeds() {
        ResponseEntity<List<Fraction>> postResponse = restTemplate.exchange("/fractions", HttpMethod.POST, new HttpEntity<>(createFractions()), responseType);
        assertEquals(HttpStatus.OK, postResponse.getStatusCode());
        Fraction fraction = postResponse.getBody().get(0);
        UUID id = fraction.getId();
        ResponseEntity<Fraction> getResponse = restTemplate.getForEntity("/fractions/{id}", Fraction.class, id);
        assertEquals(HttpStatus.OK, getResponse.getStatusCode());
        assertEquals(fraction, getResponse.getBody());
    }

    @Test
    public void testDeleteFails() {
        ResponseEntity<String> response = restTemplate
                .exchange("/fractions/{id}", HttpMethod.DELETE, new HttpEntity<>(new HttpHeaders()), String.class, UUID.randomUUID());
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    public void testDeleteSucceeds() {
        ResponseEntity<List<Fraction>> postResponse = restTemplate.exchange("/fractions", HttpMethod.POST, new HttpEntity<>(createFractions()), responseType);
        assertEquals(HttpStatus.OK, postResponse.getStatusCode());
        Fraction fraction = postResponse.getBody().get(0);
        UUID id = fraction.getId();
        ResponseEntity<String> deleteResponse = restTemplate
                .exchange("/fractions/{id}", HttpMethod.DELETE, new HttpEntity<>(new HttpHeaders()), String.class, id);
        assertEquals(HttpStatus.NO_CONTENT, deleteResponse.getStatusCode());
    }

    @Test
    public void testCreateFractionsOk() {
        ResponseEntity<List<Fraction>> response = restTemplate.exchange("/fractions", HttpMethod.POST, new HttpEntity<>(createFractions()), responseType);
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testCreateFractionsWrongSumShouldFail() throws JSONException {
        ResponseEntity<String> response = restTemplate.postForEntity("/fractions", createFractionsWithWrongFractionSum(), String.class);
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        JSONAssert.assertEquals("{\"violations\":[{\"fieldName\":\"fractions\",\"message\":\"The sum of the fractions is not equal to 1.0\"}]}", response.getBody(), false);
    }

    @Test
    public void testCreateFractionsInvalidMonthsShouldFail() throws JSONException {
        ResponseEntity<String> response = restTemplate.postForEntity("/fractions", createFractionsNotEnoughMonths(), String.class);
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        JSONAssert.assertEquals(
                "{\"violations\":[{\"fieldName\":\"fractions\",\"message\":\"Wrong amount of months, 11 is not equal to 12\"},{\"fieldName\":\"fractions\",\"message\":\"The sum of the fractions is not equal to 1.0\"}]}",
                response.getBody(), false);
    }

    @Test
    public void testMultipleProfilesOk() {
        List<Fraction> fractions = List
                .of(new Fraction("ABC", createFractionMap()), new Fraction("DEF", createFractionMap()), new Fraction("XYZ", createFractionMap()));
        ResponseEntity<String> response = restTemplate.postForEntity("/fractions", fractions, String.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    private List<Fraction> createFractionsNotEnoughMonths() {
        Map<Month, Double> fractions = createFractionMap();
        fractions.remove(Month.JANUARY);
        return List.of(new Fraction("ABC", fractions));
    }

    private List<Fraction> createFractionsWithWrongFractionSum() {
        Map<Month, Double> fractions = createFractionMap();
        fractions.put(Month.JANUARY, 0.9);
        return List.of(new Fraction("ABC", fractions));
    }

    public static List<Fraction> createFractions() {
        return List.of(new Fraction("ABC", createFractionMap()));
    }

    private static Map<Month, Double> createFractionMap() {
        Map<Month, Double> fractions = new HashMap<>();
        fractions.put(Month.JANUARY, 0.1);
        fractions.put(Month.FEBRUARY, 0.1);
        fractions.put(Month.MARCH, 0.1);
        fractions.put(Month.APRIL, 0.1);
        fractions.put(Month.MAY, 0.1);
        fractions.put(Month.JUNE, 0.1);
        fractions.put(Month.JULY, 0.1);
        fractions.put(Month.AUGUST, 0.1);
        fractions.put(Month.SEPTEMBER, 0.05);
        fractions.put(Month.OCTOBER, 0.05);
        fractions.put(Month.NOVEMBER, 0.05);
        fractions.put(Month.DECEMBER, 0.05);
        return fractions;
    }

}
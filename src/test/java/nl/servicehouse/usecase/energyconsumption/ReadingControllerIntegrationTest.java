package nl.servicehouse.usecase.energyconsumption;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.json.JSONException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import nl.servicehouse.usecase.energyconsumption.common.Month;
import nl.servicehouse.usecase.energyconsumption.fraction.Fraction;
import nl.servicehouse.usecase.energyconsumption.fraction.FractionRepository;
import nl.servicehouse.usecase.energyconsumption.reading.Reading;
import nl.servicehouse.usecase.energyconsumption.reading.ReadingRepository;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = EnergyConsumptionApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ReadingControllerIntegrationTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private ReadingRepository readingRepository;
    @Autowired
    private FractionRepository fractionRepository;

    private ParameterizedTypeReference<List<Reading>> responseType = new ParameterizedTypeReference<>() {

    };

    @Before
    public void setUp() {
        ParameterizedTypeReference<List<Fraction>> fractionResponseType = new ParameterizedTypeReference<>() {

        };
        ResponseEntity<List<Fraction>> postResponse = restTemplate
                .exchange("/fractions", HttpMethod.POST, new HttpEntity<>(FractionControllerIntegrationTest.createFractions()), fractionResponseType);
        assertEquals(HttpStatus.OK, postResponse.getStatusCode());
    }

    @After
    public void tearDown() {
        //Manually clear the database
        //@Transactional doesnt work since we dont call services but do HTTP calls with RestTemplate
        fractionRepository.deleteAll();
        readingRepository.deleteAll();
    }

    @Test
    public void testGetReadingsFails() {
        ResponseEntity<String> response = restTemplate.getForEntity("/readings/{id}", String.class, UUID.randomUUID());
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    public void testGetReadingsSucceeds() {
        ResponseEntity<List<Reading>> postResponse = restTemplate.exchange("/readings", HttpMethod.POST, new HttpEntity<>(createReadings()), responseType);
        assertEquals(HttpStatus.OK, postResponse.getStatusCode());
        Reading reading = postResponse.getBody().get(0);
        UUID id = reading.getId();
        ResponseEntity<Reading> getResponse = restTemplate.getForEntity("/readings/{id}", Reading.class, id);
        assertEquals(HttpStatus.OK, getResponse.getStatusCode());
        assertEquals(reading, getResponse.getBody());
    }

    @Test
    public void testCreateReadingsOk() {
        ResponseEntity<List<Reading>> postResponse = restTemplate.exchange("/readings", HttpMethod.POST, new HttpEntity<>(createReadings()), responseType);
        assertEquals(HttpStatus.OK, postResponse.getStatusCode());
    }

    @Test
    public void testCreateReadingsShouldFailWhenProfileDoesntExit() throws JSONException {
        List<Reading> readings = List.of(new Reading("00001", "XYZ", createReadingsMap()));

        ResponseEntity<String> postResponse = restTemplate.postForEntity("/readings", readings, String.class);
        System.out.println(postResponse);
        assertEquals(HttpStatus.BAD_REQUEST, postResponse.getStatusCode());
        JSONAssert.assertEquals("{\"violations\":[{\"fieldName\":\"profileName\",\"message\":\"The given profile doesn't exist\"}]}", postResponse.getBody(), false);
    }

    @Test
    public void testCreateReadingsNonIncreasingReadingsShouldFail() throws JSONException {
        ResponseEntity<String> response = restTemplate.postForEntity("/readings", createReadingsNotIncremental(), String.class);
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        System.out.println(response.getBody());
        JSONAssert.assertEquals("{\"violations\":[{\"fieldName\":\"readings\",\"message\":\"Invalid meter reading, readings are not incremental\"}]}", response.getBody(), false);
    }

    @Test
    public void testCreateReadingsConsumptionTooLowShouldFail() throws JSONException {
        ResponseEntity<String> response = restTemplate.postForEntity("/readings", createReadingsConsumptionTooLow(), String.class);
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        System.out.println(response.getBody());
        JSONAssert.assertEquals("{\"violations\":[{\"fieldName\":\"readings\",\"message\":\"Invalid meter reading, readings not within expected range\"}]}", response.getBody(), false);
    }

    @Test
    public void testCreateReadingsConsumptionTooHighShouldFail() throws JSONException {
        ResponseEntity<String> response = restTemplate.postForEntity("/readings", createReadingsConsumptionTooHigh(), String.class);
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        System.out.println(response.getBody());
        JSONAssert.assertEquals("{\"violations\":[{\"fieldName\":\"readings\",\"message\":\"Invalid meter reading, readings not within expected range\"}]}", response.getBody(), false);
    }

    @Test
    public void testCreateReadingsNotEnoughMonthsShouldFail() throws JSONException {
        ResponseEntity<String> response = restTemplate.postForEntity("/readings", createReadingsNotEnoughMonths(), String.class);
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        System.out.println(response.getBody());
        JSONAssert.assertEquals("{\"violations\":[{\"fieldName\":\"readings\",\"message\":\"Reading for JANUARY is missing\"}]}", response.getBody(), false);
    }

    private List<Reading> createReadings() {
        return List.of(new Reading("00001", "ABC", createReadingsMap()));
    }

    private List<Reading> createReadingsNotEnoughMonths() {
        Map<Month, Integer> readingsMap = createReadingsMap();
        readingsMap.remove(Month.JANUARY);
        return List.of(new Reading("00001", "ABC", readingsMap));
    }

    private List<Reading> createReadingsNotIncremental() {
        Map<Month, Integer> readingsMap = createReadingsMap();
        readingsMap.put(Month.JUNE, 45); return List.of(new Reading("00001", "ABC", readingsMap));
    }

    private List<Reading> createReadingsConsumptionTooLow() {
        Map<Month, Integer> readingsMap = createReadingsMap();
        readingsMap.put(Month.JANUARY, 7);
        return List.of(new Reading("00001", "ABC", readingsMap));
    }

    private List<Reading> createReadingsConsumptionTooHigh() {
        Map<Month, Integer> readingsMap = createReadingsMap();
        readingsMap.put(Month.JANUARY, 13);
        return List.of(new Reading("00001", "ABC", readingsMap));
    }

    private Map<Month, Integer> createReadingsMap() {
        Map<Month, Integer> readings = new HashMap<>();
        readings.put(Month.JANUARY, 10);
        readings.put(Month.FEBRUARY, 20);
        readings.put(Month.MARCH, 30);
        readings.put(Month.APRIL, 40);
        readings.put(Month.MAY, 50);
        readings.put(Month.JUNE, 60);
        readings.put(Month.JULY, 70);
        readings.put(Month.AUGUST, 80);
        readings.put(Month.SEPTEMBER, 85);
        readings.put(Month.OCTOBER, 90);
        readings.put(Month.NOVEMBER, 95);
        readings.put(Month.DECEMBER, 100);
        return readings;
    }
}

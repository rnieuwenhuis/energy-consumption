package nl.servicehouse.usecase.energyconsumption.reading;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

import nl.servicehouse.usecase.energyconsumption.fraction.FractionService;

public class ProfileExistsValidator implements ConstraintValidator<ProfileExists, String> {

    @Autowired
    private FractionService fractionService;

    @Override
    public boolean isValid(String profile, ConstraintValidatorContext context) {
        return fractionService.findByProfile(profile).isPresent();
    }
}

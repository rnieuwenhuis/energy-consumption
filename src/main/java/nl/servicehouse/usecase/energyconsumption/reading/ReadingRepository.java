package nl.servicehouse.usecase.energyconsumption.reading;

import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ReadingRepository extends JpaRepository<Reading, UUID> {

    Optional<Reading> findByMeterId(String meterId);
}

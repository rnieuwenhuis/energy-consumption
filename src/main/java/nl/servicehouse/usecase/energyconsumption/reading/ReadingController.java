package nl.servicehouse.usecase.energyconsumption.reading;

import java.util.List;
import java.util.UUID;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import nl.servicehouse.usecase.energyconsumption.common.NotFoundException;

@RestController
@RequestMapping("/readings")
public class ReadingController {

    private final ReadingService readingService;

    public ReadingController(ReadingService readingService) {
        this.readingService = readingService;
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<Reading> get(@PathVariable UUID id) {
        Reading found = readingService.findById(id).orElseThrow(NotFoundException::new);
        return ResponseEntity.ok(found);
    }

    @PostMapping
    public ResponseEntity<List<Reading>> post(@RequestBody List<Reading> readings) {
        List<Reading> saved = readingService.save(readings);
        return ResponseEntity.ok(saved);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity delete(@PathVariable UUID id) {
        readingService.delete(id);
        return ResponseEntity.noContent().build();

    }

}

package nl.servicehouse.usecase.energyconsumption.reading;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;

import org.springframework.stereotype.Service;

import nl.servicehouse.usecase.energyconsumption.common.Month;
import nl.servicehouse.usecase.energyconsumption.common.NotFoundException;
import nl.servicehouse.usecase.energyconsumption.consumption.Consumption;
import nl.servicehouse.usecase.energyconsumption.fraction.FractionService;

@Service
public class ReadingService {

    private final ReadingRepository readingRepository;
    private final Validator validator;
    private final FractionService fractionService;

    public ReadingService(ReadingRepository readingRepository, Validator validator, FractionService fractionService) {
        this.readingRepository = readingRepository;
        this.validator = validator;
        this.fractionService = fractionService;
    }

    public Optional<Reading> findById(UUID id) {
        return readingRepository.findById(id);
    }

    public List<Reading> save(List<Reading> readings) {
        Set<ConstraintViolation<Reading>> violations = new HashSet<>();
        List<Reading> savedReadings = new ArrayList<>();

        for (Reading reading : readings) {
            Set<ConstraintViolation<Reading>> readingViolations = validator.validate(reading);
            if (readingViolations.isEmpty()) {
                savedReadings.add(readingRepository.save(reading));
            } else {
                violations.addAll(readingViolations);
            }
        }
        if (!violations.isEmpty()) {
            throw new ConstraintViolationException(violations);
        }
        readingRepository.flush();
        return savedReadings;
    }

    public void delete(UUID id) {
        readingRepository.deleteById(id);
    }

    public Consumption getConsumptionForMeter(String meterId, Month month) {
        Optional<Reading> meterReading = readingRepository.findByMeterId(meterId);
        if (meterReading.isEmpty()) {
            throw new NotFoundException();
        }
        Map<Month, Integer> readings = meterReading.get().getReadings();

        Month previousMonth = month.getPreviousMonth();
        int previousReading = 0;
        if (previousMonth != null) {
            previousReading = readings.get(previousMonth);
        }

        int consumption = readings.get(month) - previousReading;
        return new Consumption(meterId, month, consumption);
    }
}

package nl.servicehouse.usecase.energyconsumption.reading;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.hibernate.validator.constraintvalidation.HibernateConstraintValidatorContext;
import org.springframework.beans.factory.annotation.Autowired;

import nl.servicehouse.usecase.energyconsumption.common.Month;
import nl.servicehouse.usecase.energyconsumption.fraction.Fraction;
import nl.servicehouse.usecase.energyconsumption.fraction.FractionService;

public class ReadingsValidator implements ConstraintValidator<ValidReadings, Reading> {

    @Autowired
    private FractionService fractionService;

    @Override
    public boolean isValid(Reading reading, ConstraintValidatorContext context) {
        Optional<Fraction> fractions = fractionService.findByProfile(reading.getProfile());
        if (fractions.isEmpty()) {
            //no profile found, we can't validate the readings
            return true;
        }
        Fraction fraction = fractions.get();
        int totalConsumption = reading.getReadings().get(Month.DECEMBER);

        //divide the consumption over the months based on the fraction for that month
        Map<Month, Double> expectedConsumption = new HashMap<>();
        Arrays.asList(Month.values()).forEach(month -> expectedConsumption.put(month, totalConsumption * fraction.getFractions().get(month)));

        int previousReading = 0;
        for (Month month : Month.values()) {
            if (!reading.getReadings().containsKey(month)) {
                if (context instanceof HibernateConstraintValidatorContext) {
                    context.unwrap(HibernateConstraintValidatorContext.class).addMessageParameter("month", month);
                }
                return customConstraintViolations(context, "{readings.months.invalid}");
            }
            Integer currentReading = reading.getReadings().get(month);
            if (currentReading < previousReading) {
                return customConstraintViolations(context, "{readings.not.incremental}");
            }
            Integer consumption = currentReading - previousReading;
            Double expectedMonthConsumption = expectedConsumption.get(month);
            if (consumption < (expectedMonthConsumption * 0.75) || consumption > (expectedMonthConsumption * 1.25)) {
                return customConstraintViolations(context, "{readings.consumption.invalid}");
            }
            previousReading = currentReading;
        }
        return true;
    }

    private boolean customConstraintViolations(ConstraintValidatorContext context, String messageTemplate) {
        context.disableDefaultConstraintViolation();
        context.buildConstraintViolationWithTemplate(messageTemplate).addPropertyNode("readings").addConstraintViolation();
        return false;
    }
}

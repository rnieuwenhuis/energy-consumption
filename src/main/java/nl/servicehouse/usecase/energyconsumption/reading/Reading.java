package nl.servicehouse.usecase.energyconsumption.reading;

import java.util.Collections;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.MapKeyClass;
import javax.persistence.MapKeyEnumerated;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.data.jpa.domain.AbstractPersistable;

import com.fasterxml.jackson.annotation.JsonProperty;

import nl.servicehouse.usecase.energyconsumption.common.Month;

@Entity
@ValidReadings
public class Reading extends AbstractPersistable<UUID> {

    @Column(unique = true)
    @NotNull
    @JsonProperty("meterId")
    private String meterId;

    @NotNull
    @ProfileExists
    @JsonProperty("profileName")
    private String profileName;

    @ElementCollection
    @MapKeyClass(Month.class)
    @MapKeyEnumerated(EnumType.STRING)
    @NotEmpty
    private Map<Month, Integer> readings;

    private Reading() {
    }

    public Reading(String meterId, String profileName, Map<Month, Integer> readings) {
        this.meterId = meterId;
        this.profileName = profileName;
        this.readings = readings;
    }

    public String getMeterId() {
        return meterId;
    }

    public String getProfile() {
        return profileName;
    }

    public Map<Month, Integer> getReadings() {
        return Collections.unmodifiableMap(readings);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        Reading other = (Reading) obj;
        return Objects.equals(meterId, other.meterId) && Objects.equals(profileName, other.profileName) && Objects.equals(readings, other.readings);
    }

    @Override
    public int hashCode() {
        return Objects.hash(meterId, profileName, readings);
    }
}

package nl.servicehouse.usecase.energyconsumption.consumption;

import nl.servicehouse.usecase.energyconsumption.common.Month;

public class Consumption {

    private final String meterId;
    private final Month month;
    private final int consumption;

    public Consumption(String meterId, Month month, int consumption) {
        this.meterId = meterId;
        this.month = month;
        this.consumption = consumption;
    }

    public String getMeterId() {
        return meterId;
    }

    public Month getMonth() {
        return month;
    }

    public int getConsumption() {
        return consumption;
    }
}

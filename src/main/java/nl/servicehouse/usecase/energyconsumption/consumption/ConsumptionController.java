package nl.servicehouse.usecase.energyconsumption.consumption;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import nl.servicehouse.usecase.energyconsumption.common.Month;
import nl.servicehouse.usecase.energyconsumption.reading.ReadingService;

@RestController
@RequestMapping("/consumption")
public class ConsumptionController {

    private final ReadingService readingService;

    public ConsumptionController(ReadingService readingService) {
        this.readingService = readingService;
    }

    @GetMapping(value = "/{meterId}/{month}")
    public ResponseEntity<Consumption> get(@PathVariable String meterId, @PathVariable Month month) {
        Consumption consumption = readingService.getConsumptionForMeter(meterId, month);
        return ResponseEntity.ok(consumption);
    }

}

package nl.servicehouse.usecase.energyconsumption.common;

import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum Month {

    JANUARY("JAN"),
    FEBRUARY("FEB"),
    MARCH("MAR"),
    APRIL("APR"),
    MAY("MAY"),
    JUNE("JUN"),
    JULY("JUL"),
    AUGUST("AUG"),
    SEPTEMBER("SEP"),
    OCTOBER("OCT"),
    NOVEMBER("NOV"),
    DECEMBER("DEC");

    private final String shortName;

    Month(String shortName) {
        this.shortName = shortName;
    }

    @JsonValue
    public String getShortName() {
        return shortName;
    }

    public Month getPreviousMonth() {
        if (this == JANUARY) {
            return null;
        }
        return Month.values()[this.ordinal() - 1];
    }

    public static Month fromShortName(String value) {
        return Arrays.stream(Month.values()).filter(month -> month.getShortName().equals(value)).findFirst().orElseThrow(IllegalArgumentException::new);
    }
}

package nl.servicehouse.usecase.energyconsumption.common;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class MonthConverter implements Converter<String, Month> {

    @Override
    public Month convert(String value) {
        return Month.fromShortName(value);
    }
}
package nl.servicehouse.usecase.energyconsumption.common;

public class NotFoundException extends RuntimeException {

    public NotFoundException() {
        super("Entity not found");
    }
}
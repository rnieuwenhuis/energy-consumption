package nl.servicehouse.usecase.energyconsumption.fraction;

import java.util.Map;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import nl.servicehouse.usecase.energyconsumption.common.Month;

public class CorrectSumValidator implements ConstraintValidator<CorrectSum, Map<Month, Double>> {

    @Override
    public boolean isValid(Map<Month, Double> fractions, ConstraintValidatorContext context) {
        return fractions.values().stream().mapToDouble(Double::doubleValue).sum() == 1.0;
    }
}

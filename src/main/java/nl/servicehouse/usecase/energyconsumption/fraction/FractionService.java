package nl.servicehouse.usecase.energyconsumption.fraction;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class FractionService {

    private final FractionRepository fractionRepository;
    private final Validator validator;

    public FractionService(FractionRepository fractionRepository, Validator validator) {
        this.fractionRepository = fractionRepository;
        this.validator = validator;
    }

    public List<Fraction> save(List<Fraction> fractions) {
        Set<ConstraintViolation<Fraction>> violations= fractions.stream()
                                                                .map(fraction -> validator.validate(fraction))
                                                                .flatMap(Set::stream)
                                                                .collect(Collectors.toSet());
        if (violations.isEmpty()) {
            return fractionRepository.saveAll(fractions);
        }
        throw new ConstraintViolationException(violations);
    }

    public Optional<Fraction> findById(UUID id) {
        return fractionRepository.findById(id);
    }

    public void delete(UUID id) {
        fractionRepository.deleteById(id);
    }

    public Optional<Fraction> findByProfile(String profile) {
        return fractionRepository.findByProfileName(profile);
    }
}

package nl.servicehouse.usecase.energyconsumption.fraction;

import java.util.List;
import java.util.UUID;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import nl.servicehouse.usecase.energyconsumption.common.NotFoundException;

@RestController
@RequestMapping("/fractions")
public class FractionController {

    private final FractionService fractionService;

    public FractionController(FractionService fractionService) {
        this.fractionService = fractionService;
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<Fraction> get(@PathVariable UUID id) {
        Fraction found = fractionService.findById(id).orElseThrow(NotFoundException::new);
        return ResponseEntity.ok(found);
    }

    @PostMapping
    public ResponseEntity<List<Fraction>> post(@RequestBody List<Fraction> fractions) {
        List<Fraction> saved = fractionService.save(fractions);
        return ResponseEntity.ok(saved);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity delete(@PathVariable UUID id) {
        fractionService.delete(id);
        return ResponseEntity.noContent().build();

    }

}

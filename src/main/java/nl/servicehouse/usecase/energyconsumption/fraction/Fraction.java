package nl.servicehouse.usecase.energyconsumption.fraction;

import java.util.Collections;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.MapKeyClass;
import javax.persistence.MapKeyEnumerated;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.data.jpa.domain.AbstractPersistable;

import com.fasterxml.jackson.annotation.JsonProperty;

import nl.servicehouse.usecase.energyconsumption.common.Month;

@Entity
public class Fraction extends AbstractPersistable<UUID> {

    @NotNull
    @JsonProperty("profileName")
    private String profileName;

    @ElementCollection
    @MapKeyClass(Month.class)
    @MapKeyEnumerated(EnumType.STRING)
    @NotEmpty
    @CorrectSum
    @CorrectMonths
    private Map<Month, Double> fractions;

    private Fraction() {

    }

    public Fraction(String profileName, Map<Month, Double> fractions) {
        this.profileName = profileName;
        this.fractions = fractions;
    }

    public String getName() {
        return profileName;
    }

    public Map<Month, Double> getFractions() {
        return Collections.unmodifiableMap(fractions);
    }

    @Override
    public int hashCode() {
        return Objects.hash(profileName, fractions);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        Fraction other = (Fraction) obj;
        return Objects.equals(profileName, other.profileName) && Objects.equals(fractions, other.fractions);
    }
}

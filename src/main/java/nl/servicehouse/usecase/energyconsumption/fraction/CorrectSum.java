package nl.servicehouse.usecase.energyconsumption.fraction;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Target({ FIELD })
@Retention(RUNTIME)
@Constraint(validatedBy = CorrectSumValidator.class)
@Documented
public @interface CorrectSum {

    String message() default "{fractions.sum.invalid}";

    Class<?>[] groups() default { };

    Class<? extends Payload>[] payload() default { };

}

package nl.servicehouse.usecase.energyconsumption.fraction;

import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

public interface FractionRepository extends JpaRepository<Fraction, UUID> {

    Optional<Fraction> findByProfileName(String profile);
}

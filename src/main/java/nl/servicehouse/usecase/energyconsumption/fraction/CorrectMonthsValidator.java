package nl.servicehouse.usecase.energyconsumption.fraction;

import java.util.List;
import java.util.Map;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.hibernate.validator.constraintvalidation.HibernateConstraintValidatorContext;

import nl.servicehouse.usecase.energyconsumption.common.Month;

public class CorrectMonthsValidator implements ConstraintValidator<CorrectMonths, Map<Month, Double>> {

    @Override
    public boolean isValid(Map<Month, Double> fractions, ConstraintValidatorContext context) {
        if (fractions.size() != 12) {
            if (context instanceof HibernateConstraintValidatorContext) {
                context.unwrap(HibernateConstraintValidatorContext.class).addMessageParameter("value", fractions.size());
            }
            return customConstraintViolations(context, "{fractions.months.amount}");
        }
        return true;
    }

    private boolean customConstraintViolations(ConstraintValidatorContext context, String messageTemplate) {
        context.disableDefaultConstraintViolation();
        context.buildConstraintViolationWithTemplate(messageTemplate).addConstraintViolation();
        return false;
    }

}
